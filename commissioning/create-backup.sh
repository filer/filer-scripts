#!/bin/bash -x

# Use this as

# ./create-backup-vm.sh
# to create a VM with hostname filer-backup-<random>.cern.ch

unset OS_PROJECT_ID;
unset OS_TENANT_ID;
unset OS_TENANT_NAME;

export OS_PROJECT_NAME="IT Filer Service";
export OS_REGION_NAME="pdc"

openstack server list  > /dev/null 2>&1
if [ $? != "0" ] ; then
  echo "openstack server list not working"
  exit
fi

# TODO: Maybe add anti affinity?
#
# Availability zones are not available in PDC.
# Let's use soft-anti-affinity to make sure the VMs do not land on the same hypervisor
#
# Affinity and anti-affinity filters extended with zone (rack) and location (room)
#   – soft vs hard (cloud prefers soft as it gives them more leeway if they need to migrate VMs)
#   – scope: Host, Zone (rack), Location (room)
#   – max_server_per_scope
#
#   $ openstack server group create \
#       --policy soft-anti-affinity \
#       --rule scope=zone \
#       --rule max_server_per_scope=1 \
#       one_per_rack
#
# If desirable, add a nova-hint for the created server group:
#   `--nova-hint 'group=<uuid-of-the-server-group>' \`
#

PREFIX="filer-backup-"

ai-bs     --landb-mainuser 'nfs-admins' \
          --landb-responsible 'nfs-admins' \
          --nova-flavor 'm4.large' \
          --rhel9 \
          --foreman-environment 'production' \
          --foreman-hostgroup 'filer/spare' \
	  --prefix $PREFIX

