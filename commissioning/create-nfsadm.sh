#!/bin/bash -x

# Use this as

# ./create-nfsadm to create a VM with hostname nfsadm-<whatever>.cern.ch

unset OS_PROJECT_ID;
unset OS_TENANT_ID;
unset OS_TENANT_NAME;

export OS_PROJECT_NAME="IT Filer Service";

openstack server list  > /dev/null 2>&1
if [ $? != "0" ] ; then
  echo "openstack server list not working"
  exit
fi

ai-bs     --landb-mainuser 'nfs-admins' \
          --landb-responsible 'nfs-admins' \
          --nova-flavor 'm2.small' \
          --rhel9 \
          --foreman-environment 'production' \
	  --foreman-hostgroup 'filer/nfsadm' \
	  --prefix nfsadm-

