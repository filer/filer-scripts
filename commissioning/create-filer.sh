#!/bin/bash -x

# Use this as

# ./create-filer.sh 12a
# to create a VM with hostname itnfs12a.cern.ch


NUMBER=$1
if [ x"$NUMBER" == x"" ];
then
  echo "Error: VM number not provided."
  echo "Usage: ./create-filer.sh <number>"
  exit 1
fi


unset OS_PROJECT_ID;
unset OS_TENANT_ID;
unset OS_TENANT_NAME;

## export OS_PROJECT_NAME="IT Filer Service";
export OS_PROJECT_NAME="IT Filer Service - critical area"
export OS_REGION_NAME="cern"


openstack server list  > /dev/null 2>&1
if [ $? != "0" ] ; then
  echo "openstack server list not working"
  exit
fi

VMNAME="itnfs$NUMBER.cern.ch"
echo "About to create filer VM with name $VMNAME"
echo "  Interrupt now if wrong..."
echo
sleep 5

FLAVOR='m2.xlarge'
## FLAVOR='m2.2xlarge'

ai-bs     --landb-mainuser 'nfs-admins' \
          --landb-responsible 'nfs-admins' \
          --nova-flavor $FLAVOR \
          --rhel9 \
          --foreman-environment 'production' \
          --foreman-hostgroup 'filer/spare' \
	  $VMNAME
